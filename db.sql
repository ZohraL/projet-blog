DROP DATABASE IF EXISTS blog; 
CREATE DATABASE blog;
USE blog; 
CREATE TABLE post(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(64),
    author VARCHAR(64), 
    content TEXT
);

INSERT INTO post (title, author, content) VALUES ('Bienvenue', 'Zohra', 'Welcome on Board');