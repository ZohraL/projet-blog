<?php

namespace App\Entity;

use DateTime;

class Post
{
    private $id;
    private $title;
    private $author;
    private $postDate;
    private $content;

    public function __construct(string $title, string $author, string $content, string $postDate = 'now', int $id = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->postDate = new \DateTime($postDate);
        $this->content = $content;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
    public function getAuthor(): string
    {
        return $this->author;
    }
    public function getContent(): string
    {
        return $this->content;
    }
    public function getPostDate(): \DateTime
    {
        return $this->postDate;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
