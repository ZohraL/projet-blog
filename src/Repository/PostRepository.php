<?php

namespace App\Entity\Post;

use App\Entity\Post;

class PostRepository
{


    private $pdo;
    public function __construct()
    {
        $this->pdo = new \PDO(
            'mysql:host=localhost;dbname=blog',
            'simplon',
            '1234'
        );
    }
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM post');
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
        }
        $list[] = $post;
        return $list;
    }

    public function add(Post $post): void
    {
        $query = $this->pdo->prepare('INSERT INTO post (title, author, content, postDate) VALUES (:title,:author,:content, :postDate)');
        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);
        $query->bindValue('postDate', $post->getPostDate()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);


        $query->execute();
        $post->setId(intval($this->pdo->lastInsertId()));
    }

    public function findById(int $id): ?Post
    {
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if ($line) {
            return $this->sqlToPost($line);
        }
        return null;
    }
    private function sqlToPost(array $line): Post
    {
        return new Post($line['title'], $line['author'], $line['content'], $line['postDate'], $line['id']);
    }
}
